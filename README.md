# Cardona lab data policy

Work in progress.

This is a document set out for Cardona lab members to maximise the discoverability, compatibility, legibility, and longevity of data, along with FAIR principles (Findable, Accessible, Interoperable, Reusable).

v1.0 of this policy was presented to the Cardona and Zlatic labs 2020-10-07.

[`FILE_STRUCTURE.md`](./FILE_STRUCTURE.md) contains the specification of the Flexible File Structure.
[`GUIDELINES.md`](./GUIDELINES.md) contains general guidelines for FAIR data storage.

## Tools

[`flexfs`](https://pypi.org/project/flexfs/) ([github](https://github.com/clbarnes/ffs)) is a python package providing the `ffs` library and command-line tool for querying the structure set out here.

